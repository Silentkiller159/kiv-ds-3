# KIV-DS-3 - Cluster of MQTT Brokers with Zookeeper

# Zadání

Navrhněte architekturu distribuovaného message brokeru s využitím Apache Zookeeper a MQTT brokeru Mosquitto.  
Cílem je navrhnout cluster MQTT brokerů, který je schopen transparentně obsloužit velké množství MQTT klientů.

Technické detaily:
- Klient nezná IP adresy MQTT brokerů, pouze referenci/jméno clusteru přes níž je schopen obdržet seznam funkčních MQTT brokerů v clusteru
- Pokud MQTT broker vypadne, je klient schopen přejít na jiný funkční
- Je možné provozovat více clusterů najednou

# Architektura

![](./img/kiv-ds-3-graph.drawio.png)

# Dodatečné informacce

Architektura obsahuje 3 hlavní bloky, všechny budou jako provozovány jako Docker kontejnery:

- MQTT Broker
  - Bude použit Mosquitto
  - Řídící program bude napsán v Pythonu
    - Provede registraci Brokeru v Zookeeperu
    - Drží si seznam ostatních brokerů
    - Syncronizuje zaslané zprávy mezi brokery

- MQTT Client
  - Bude použit program v Pythonu (Pravděpodobně s knihovnou pro MQTT komunikaci)
  - Zeptá se Zookeperu na dostupné Brokery
  - Vybere si z dostupných Brokerů a naváže spojení
  - V případě výpadku spojení se celá sekvence hledání Brokera opakuje

- Zookeeper
  - Apache Zookeeper
  - Cluster 3 instancí
  - Drží si informace o aktivních MQTT brokerech

